freefem (3.5.8-7) unstable; urgency=medium

  * QA upload.
  * Set maintainer to the QA team.
  * Install *.svg not *.gif, fixes FTBFS (Closes: #906356)

 -- Adam Borowski <kilobyte@angband.pl>  Thu, 20 Sep 2018 02:09:22 +0200

freefem (3.5.8-6) unstable; urgency=medium

  * Team upload.
  * Moved packaging from SVN to Git
  * cme fix dpkg-control
  * convert from cdbs to dh
  * Standards-Version: 4.1.3

 -- Andreas Tille <tille@debian.org>  Wed, 24 Jan 2018 17:09:48 +0100

freefem (3.5.8-5.1) unstable; urgency=low

  * Non-maintainer upload.
  [Hideki Yamane]
  * debian/rules
    - specify "-fi" option to autoreconf (Closes: #724199)

 -- YunQiang Su <syq@debian.org>  Thu, 17 Jul 2014 22:08:19 +0800

freefem (3.5.8-5) unstable; urgency=low

  * Team uploaded
  * Use doxygen-latex instead of texlive-latex-base
    texlive-latex-recommended (Closes: #616214)
  * Standards-Version updated to version 3.9.2
  * Package switched to the debian science team
  * Update of the copyright file
  * Missing debhelper-but-no-misc-depends fixed
  * la file removed
  * description-synopsis-starts-with-article fixed

  [ Rafael Laboissiere ]
  * debian/control: Added Vcs-Svn and Vcs-Browser fields to the Source section

  [ Daniel Leidert ]
  * debian/control: Put packages into the correct sections. Set Homepage
    field.
    (Standards-Version): Updated to latest version 3.7.3.
    (Depends, Suggests): Cleaned up. Replaced Source-Version variables.
  * debian/copyright: Added license information.
  * debian/dirs: Removed.
  * debian/docs: Removed.
  * debian/freefem.1: Fixed a GROFF issue to make lintian happy. Small
    improvements to the content and code.
  * debian/freefem-doc.doc-base (Section): Fixed for doc-base 0.8.10.
  * debian/rules: Set DEB_INSTALL_DOCS_ALL in favour of debian/docs.
  * debian/watch: Added.

  [ Sylvestre Ledru ]
  * Switch to dpkg-source 3.0 (quilt) format

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 02 Aug 2011 14:46:54 +0200

freefem (3.5.8-4.2) unstable; urgency=low

  * Non-maintainer upload.
  * debian/control:
    - add ${shlibs:Depends} (Closes: #553132)
    - use ${source:Version} or ${binary:Version} instead of ${Source-Version},
      which is deprecated

 -- David Paleino <dapal@debian.org>  Sun, 15 Nov 2009 10:27:39 +0100

freefem (3.5.8-4.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix gcc-4.3 FTBFS, patch by Cyril Brulebois (Closes: #455278)

 -- Marc 'HE' Brockschmidt <he@debian.org>  Sun, 16 Mar 2008 17:38:46 +0100

freefem (3.5.8-4) unstable; urgency=low

  [ Rafael Laboissiere ]
  * debian/control:
    + Replaced build-dependencies on teTeX by the corresponding TeXLive ones
    + Removed useless build-dependency on doc-base
  * doc/freefem.tex: Use fancyhdr.sty instead of the deprecated
    fancyheadings.sty (closes: #421071)

  [ Andreas Barth ]
  * Actually upload.

 -- Andreas Barth <aba@not.so.argh.org>  Sat,  9 Jun 2007 22:34:17 +0000

freefem (3.5.8-3) unstable; urgency=low

  * Build-Depends on automake1.9

 -- Christophe Prud'homme <prudhomm@debian.org>  Mon, 31 Jul 2006 15:34:38 +0200

freefem (3.5.8-2) unstable; urgency=low

  * Fix compilation

 -- Christophe Prud'homme <prudhomm@debian.org>  Mon, 31 Jul 2006 14:52:43 +0200

freefem (3.5.8-1) unstable; urgency=low

  * New Upstream release (minor fixes and applied debian patches)
  * Bug fix: "freefem: FTBFS: bashisms", thanks to Julien Danjou (Closes:
    #378555).
  * Bug fix: "packaged as a native package", thanks to Matthias Klose
    (Closes: #315246).
  * Bug fix: "freefem: package description typo(s) and the like", thanks
    to Florian Zumbiehl (Closes: #299980).
  * Bug fix: "freefem-examples: package description typo(s) and the like",
    thanks to Florian Zumbiehl (Closes: #299982).
  * Bug fix: "freefem-doc: package description typo(s) and the like",
    thanks to Florian Zumbiehl (Closes: #299986).
  * Bug fix: "libfreefem-dev: package description typo(s) and the like",
    thanks to Florian Zumbiehl (Closes: #300005).
  * Bug fix: "libfreefem-doc: package description typo(s) and the like",
    thanks to Florian Zumbiehl (Closes: #300009).
  * Bug fix: "libfreefem0: package description typo(s) and the like",
    thanks to Florian Zumbiehl (Closes: #300021).

 -- Christophe Prud'homme <prudhomm@debian.org>  Mon, 31 Jul 2006 09:53:11 +0200

freefem (3.5.7-5) unstable; urgency=low

  * Bug fix: "freefem: FTBFS: fails to build from source", thanks to
    Aurelien Jarno (Closes: #347213).

 -- Christophe Prud'homme <prudhomm@debian.org>  Mon,  9 Jan 2006 23:37:57 +0100

freefem (3.5.7-4) unstable; urgency=low

  * Bug fix: "freefem: FTBFS: build-depends on removed xlibs-dev", thanks
    to Adeodato SimÃ³ (Closes: #346696).
  * Use pdf-viewer as a suggestion for the -doc package
  * Fixed lintian errors and some warnings
  * Fixed some linda warnings
  * Updated Standards-Version

 -- Christophe Prud'homme <prudhomm@debian.org>  Mon,  9 Jan 2006 09:14:38 +0100

freefem (3.5.7-3) unstable; urgency=low

  *  Build depends on hevea instead of latex2html using patch by Stefano
    Zacchiroli <zack@debian.org>  (closes: #221335)

 -- Christophe Prud'homme <prudhomm@mit.edu>  Fri,  9 Jan 2004 11:17:56 +0100

freefem (3.5.7-2) unstable; urgency=low

  * removed automake1.5 dependency thanks to Eric Dorland and Artur
    R. Czechowski for pointing this out. It was a remnant of an old build
    system. (closes: #205984)
  * update Standards-Version to 3.6.0.0
  * use dh_install instead of dh_movefile
  * freefem-examples depends on freefem and there is a link to the example
    in /usr/share/doc/freefem/examples .

 -- Christophe Prud'homme <prudhomm@mit.edu>  Wed, 20 Aug 2003 10:38:31 +0200

freefem (3.5.7-1) unstable; urgency=low

  * move from g++-3.2 to g++ (back to normal)
  * fix the .dhelp 0 size file problem (closes: #161738)
  * don't do make ps( do generate pdf doc) anymore when generating the doc (closes: #166894)

 -- Christophe Prud'homme <prudhomm@mit.edu>  Fri, 28 Feb 2003 10:49:43 +0100

freefem (3.5.7-pre1v1) unstable; urgency=low

  * new upstream: minor fixes
  * now build-depends on g++-3.2

 -- Christophe Prud'homme <prudhomm@mit.edu>  Mon, 23 Sep 2002 13:35:11 -0400

freefem (3.5.6-5) unstable; urgency=low

  * dont use build-depends-indep (closes: #146099)

 -- Christophe Prud'homme <prudhomm@mit.edu>  Tue,  7 May 2002 08:42:10 -0400

freefem (3.5.6-4) unstable; urgency=low

  * Argh, dh_installexamples disappeared in previous version, re-add it now
  * now depends on g++-3.0 for all arch (closes: #146074)
  * clean up control a bit more
  * removed link to /usr/doc let debhelper do it (closes: #145805)

 -- Christophe Prud'homme <prudhomm@mit.edu>  Tue,  7 May 2002 01:04:01 -0400

freefem (3.5.6-3) unstable; urgency=low

  * fix the build-dependencies for g++ for good (closes: #141779)
  * fix build of arm (closes: #133022)
  * use library versioning now: starts with libfreefem0 (closes: #141800)
  * standalone build-indeps, should be cool for auto-builders
  * put all documentation (arch-indep) into freefem-doc or libfreefem-doc
  * do not compress pdf files they are already compressed
  * added configure and doc in the .PHONY rule

 -- Christophe Prud'homme <prudhomm@mit.edu>  Tue, 23 Apr 2002 22:43:32 -0400

freefem (3.5.6-2) unstable; urgency=low

  * fix the build-dependencies for g++  (closes: #141779)

 -- Christophe Prud'homme <prudhomm@mit.edu>  Mon,  8 Apr 2002 09:24:51 -0400

freefem (3.5.6-1) unstable; urgency=low

  * sync with upstream
  * now uses g++-3.0 for all platforms
  * fixed internal compiler error on arm with g++-3.0 usage (closes: #133022)

 -- Christophe Prud'homme <prudhomm@mit.edu>  Sat,  6 Apr 2002 13:50:15 -0500

freefem (3.5.5-2) unstable; urgency=low

  * now Suggests dhelp
  * touch .dhelp in /usr/share/doc/libfreefem-doc/html and force link in /usr/doc (closes: #127146, #116756)

 -- Christophe Prud'homme <prudhomm@mit.edu>  Tue, 29 Jan 2002 20:47:19 -0500

freefem (3.5.5-1) unstable; urgency=low

  * sync with upstream
  * added some ideas from autotools README  ideas: config.{sub,guess} and
    correct arch
  * fix gcc 3.0 miscompilation thanks to sync with upstream. Fix
    micompilation on hppa architecture.(closes: #124291)

 -- Christophe Prud'homme <prudhomm@mit.edu>  Sat, 29 Dec 2001 17:11:54 -0500

freefem (3.5.4-1) unstable; urgency=low

  * sync with upstream
  * applied patch from James Stroup to upstream(closes: #115483)
  * suggest acroread for the freefem documentation manual

 -- Christophe Prud'homme <prudhomm@mit.edu>  Sat, 20 Oct 2001 15:14:15 -0400

freefem (3.5.3-2) unstable; urgency=low

  * Build-Depends on xlibs-dev (>> 4.1.0) and not xlibs(>> 4.1.0) (closes: #115231)

 -- Christophe Prud'homme <prudhomm@mit.edu>  Thu, 11 Oct 2001 15:59:27 -0400

freefem (3.5.3-1) unstable; urgency=low

  * new upstream

 -- Christophe Prud'homme <prudhomm@mit.edu>  Mon,  8 Oct 2001 00:30:10 -0400

freefem (3.5.2-1) unstable; urgency=low

  * new upstream release
  * now freefem is split in 4 packages

 -- Christophe Prud'homme <prudhomm@users.sourceforge.net>  Thu,  3 Aug 2000 17:02:23 -0400

freefem (3.5.1-1) stable; urgency=low

  * changes debain config

 -- Christophe Prud'homme <prudhomm@users.sourceforge.net>  Tue, 13 Jun 2000 20:30:37 -0400

freefem (3.4.0-1) unstable; urgency=low

  * Initial Release.
    Now it seems that I know how to build a debian package
    Hopefully it is ok now!

 -- Christophe Prud'homme <prudhomm@users.sourceforge.net>  Sat,  7 Aug 1999 17:01:41 +0200


