Source: freefem
Maintainer: Debian QA Group <packages@qa.debian.org>
Section: math
Priority: optional
Build-Depends: debhelper (>= 11~),
               dh-exec,
               libxt-dev,
               hevea,
               doxygen-latex,
               texlive-latex-base,
               texlive-latex-recommended,
               latex2html
Standards-Version: 4.2.1
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/science-team/freefem.git
Vcs-Git: https://salsa.debian.org/science-team/freefem.git
Homepage: http://kfem.sourceforge.net

Package: freefem
Architecture: any
Depends: libfreefem0 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Suggests: freefem-examples (= ${source:Version}),
          freefem-doc (= ${source:Version})
Description: PDE oriented language using Finite Element Method
 FreeFEM is a language adapted to Partial Differential equation. The
 underlying method used  is the Finite Element Method.
 This tool has been successfully used as a teaching tool and even as a 
 research tool.

Package: freefem-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Suggests: pdf-viewer
Description: Documentation for FreeFEM (html and pdf)
 FreeFEM is a language adapted to Partial Differential equation. The
 underlying method used  is the Finite Element Method.
 This tool has been successfully used as a teaching tool and even as a 
 research tool. 

Package: freefem-examples
Architecture: all
Depends: freefem,
         ${misc:Depends}
Description: Example files for FreeFEM
 FreeFEM is a language adapted to Partial Differential equation. The
 underlying method used  is the Finite Element Method.
 This tool has been successfully used as a teaching tool and even as a 
 research tool. 

Package: libfreefem0
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Conflicts: libfreefem
Provides: libfreefem
Replaces: libfreefem
Description: Shared libraries for FreeFEM
 FreeFEM is a language adapted to Partial Differential equation. The
 underlying method used  is the Finite Element Method.
 This tool has been successfully used as a teaching tool and even as a 
 research tool. 

Package: libfreefem-dev
Architecture: any
Section: libdevel
Depends: libfreefem0 (= ${binary:Version}),
         ${misc:Depends}
Suggests: libfreefem-doc (= ${source:Version})
Description: Development library, header files and manpages
 FreeFEM is a language adapted to Partial Differential equation. The
 underlying method used  is the Finite Element Method.
 This tool has been successfully used as a teaching tool and even as a 
 research tool. 

Package: libfreefem-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Description: Documentation for FreeFEM development
 FreeFEM is a language adapted to Partial Differential equation. The
 underlying method used  is the Finite Element Method.
 This tool has been successfully used as a teaching tool and even as a 
 research tool. 
