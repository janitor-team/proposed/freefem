/* -*- Mode: pde -*- */

complex;
border(1,0,1,20){ x:=5; y:=1+2*t};
border(1,0,1,20){ x:=5-2*t; y:=3};
border(1,0,1,20){ x:=3-2*t; y:=3-2*t};
border(1,0,1,10){ x:=1-t; y:=1};
border(2,0,1,10){ x:=0; y:=1-t};
border(1,0,1,10){ x:=t; y:=0};
border(1,0,1,20){ x:=1+4*t; y:=t};

buildmesh(1000);
c=I*0.895;

f=exp(-10*((x-0.5)*(x-0.5)+(y-0.5)*(y-0.5)));
g=exp(I*(0.7*x+0.7*y));
solve(u){

  onbdy(1) id(u)*c+dnu(u)=0;
  onbdy(2) u=g;
  pde(u) laplace(u)+id(u)*0.8 = 0;
};
plot(u);
imu=Im(u);
plot(imu);
