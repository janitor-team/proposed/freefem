border(1,0,1,20) { x:=t; y:=0};
border(1,0,1,20) { x:=1; y:=t};
border(2,0,1,20) { x:=1-t; y:=1};
border(1,0,1,20) { x:=0; y:=1-t};
buildmesh(1000);

eps:=1/100000;
solve(p,o) begin
  onbdy(1,2) id(p)/eps + dnu(o) = 0;
  onbdy(1) dnu(p) = 0;
  onbdy(2) dnu(p) = 1;
  pde(o) -laplace(o) =0;
  pde(p) id(o) + laplace(p) = 0;
end;

plot(o); plot(p);
        
