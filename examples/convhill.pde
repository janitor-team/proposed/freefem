changewait;
  twopi := 8 * atan(1);
   border(1,0,twopi,100) 
  {
    x := cos(t);
    y := sin(t);
  };
  buildmesh(800);
savemesh('mesh');
r2 = (x-0.3)^2 +(y-0.3)^2;
v = exp(-10*r2);
plot(v);
dt := 0.3;
nu := 0.0001;
u1 = y;
u2 = -x;

i:=1;
iter(20)
{
solve(v,i)
begin
     onbdy(1) v = 0;
      pde(v)   id(v)/dt - laplace(v)*nu =
           convect(v,u1,u2,dt)/dt;
end;
i:=-1;
plot3d(v);
}
/*save('v.dat',v);�*/
