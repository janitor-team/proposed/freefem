border(1,0,1.8,10)  begin x:=0; y:=2-t end;
border(2,pi/2,0,10) begin x:= 0.2*cos(t); y:= 0.2*sin(t) end;
border(3,0,1.8,20)   begin x:=0.2+t; y:=0 end;
border(4,0,2,10)    begin x:=2; y:=t end;
border(5,0,2,20)   begin x:=2 - t; y:= 2 end;
buildmesh(800);

E:= 2.15;
nu := 0.29;
mu :=E/(1+nu);
lambda := E*nu/((1+nu)*(1-2*nu));

solve(u,v)
{
  onbdy(1) u=0;
  onbdy(1) v=0;
  pde(u) 
    -laplace(u)*lambda - dxx(u)*mu - dxy(v)*mu = -1;
  pde(v) 
    -laplace(v)*lambda - dyx(u)*mu-dyy(v)*mu = 0;
}
plot(u); 
plot(v);
