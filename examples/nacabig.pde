border(1,0, 2 * pi,40)  begin  
x:=5*cos(t);  y:=5*sin(t)  end ;

/* 
 * the NACA0012 
 */
border(2,0,2,71) 
{
  if(t<=1) then  /* top side */
    {
      x := t;
      y := 0.17735*sqrt(t)-0.075597*t
	- 0.212836*(t^2)+0.17363*(t^3)-0.06254*(t^4);        
    }
  else  /* bottom side */
    {
      x := 2-t;
    y:= -(0.17735*sqrt(2-t)-0.075597
	  * (2-t)-0.212836*((2-t)^2)
	  +0.17363*((2-t)^3) - 0.06254*(2-t)^4);
    }
}

buildmesh(1800);

solve(psi0){
  onbdy(1) psi0 = y-0.1*x;
  onbdy(2) psi0 = 0;
  pde(psi0) -laplace(psi0) = 0;
};
plot(psi0);


solve(psi1,-1){
  onbdy(1) psi1 = 0;
  onbdy(2) psi1 = 1;
  pde(psi1) -laplace(psi1)= 0;
};
plot(psi1);

beta := psi0(0.99,0.01)+psi0(0.99,-0.01);
beta := -beta / (psi1(0.99,0.01)+ psi1(0.99,-0.01)-2);

psi = beta*psi1 +psi0;
plot(psi);
cp = - dx(psi)^2  - dy(psi)^2 ;
plot(cp);

