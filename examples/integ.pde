border(1,0,1,10,1) { x:=t;y:=0 };
border(2,0,1,10,2) { x:=1; y:=t };
border(3,0,1,10,3) { x:=1-t; y:=1 };
border(4,0,1,10,4) { x:=0; y:=1-t };

buildmesh(800);
f=x+y;
long:=int(1,2)[f]+int(3,4)[f];
surface:=intt[f];
save('result.dat',long);
save('result.dat',surface);
plot(f);