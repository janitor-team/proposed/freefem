changewait; complex;
ns:=40; 
border(1,0,2*pi,2*ns) begin x:=     3*cos(t); y:= 2*sin(t); end;
border(2,0,2*pi,ns) begin x:= cos(-t); y:= sin(-t); end;
buildmesh(ns*ns);

ue= (1+I)*sin(x+y);
p = ue;
nx = -x;
ny =- y;
dxue = (1+I)*cos(x+y);

c = 0.2;
a1 = y;
a2 = x;
nu = 1;
nu11 = 1; 
nu22 = 2;
nu21 =0.3;
nu12 =0.4;
b=1;

dnuue=dxue*(nu*(nx+ny) + 
(nu11 + nu12)*nx + (nu21+ nu22)*ny);
g = ue*c+dnuue;
f = b*ue+dxue*(a1+a2) +ue*(2*nu+nu11+nu12+nu21+nu22);

solve(u) begin
onbdy(1) u = p;
onbdy(2) id(u)*c + dnu(u) = g;
pde(u) id(u)*b + dx(u)*a1 + dy(u)*a2
-laplace(u)*nu - dxx(u)*nu11 - dxy(u)*nu12 - dyx(u)*nu21 - dyy(u)*nu22 =f;
end;

plot(u-ue);
