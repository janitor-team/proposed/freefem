
r0 := 1.0;
r1 := 2.0;
border(2,0,10,30) begin x:=t; y:=0 end;
border(2,0,1,5) begin x:=10; y:=t end;
border(2,1,2,5) begin x:=10; y:=t end;
border(2,0,10,30) begin x:=10-t; y:=2 end;
border(1,0,1,5) begin x:=0; y:=2-t end;
border(1,1,2,5) begin x:=0; y:=2-t end;
border(0,0,10,30) begin x:=t; y:=1 end;

buildmesh(800);

E= (1+10*one(y<1))*2.15;
sigma := 0.29;
mu =E/(2*(1+sigma));
lambda = E*sigma/((1+sigma)*(1-2*sigma));
nu = lambda+sigma;

solve(u,v)
begin
     onbdy(1) u=0;
     onbdy(1) v=0;
     onbdy(2) dnu(u)=0;
     onbdy(2) dnu(v)=0;
     pde(u) -laplace(u)*mu  - 
               dxx(u)*nu-dxy(v)*nu = 0;
     pde(v) -laplace(v)*mu  - 
               dyx(u)*nu-dyy(v)*nu =-0.1;
end;
plot(u); plot(v);
