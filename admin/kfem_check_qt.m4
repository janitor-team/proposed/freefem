dnl -*- Mode: m4 -*-
dnl
dnl SUMMARY:      
dnl
dnl AUTHOR:       Christophe Prud'homme
dnl ORG:          Christophe Prud'homme
dnl E-MAIL:       Christophe.Prudhomme@ann.jussieu.fr
dnl
dnl ORIG-DATE:     8-Jun-00 at 18:06:00
dnl LAST-MOD:     13-Jun-00 at 17:50:20 by Christophe Prud'homme
dnl
dnl DESCRIPTION:  
dnl This program is free software; you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation; either version 2 of the License, or
dnl  (at your option) any later version.
dnl  
dnl  This program is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with this program; if not, write to the Free Software
dnl  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
dnl DESCRIP-END.

AC_DEFUN(KFEM_CHECK_QT,
[AC_MSG_CHECKING([qt])
 ac_qt_include=qt_x11.h
 ac_qt_lib=libqt.so

 for ac_dir in \
  /usr/ \
  ;
 do
   if test -r "$ac_dir/include/qt/$ac_qt_include"; then
    ac_qt_includes=$ac_dir/include/qt
    if test -r "$ac_dir/lib/$ac_qt_lib"; then
     ac_qt_libs=$ac_dir/lib/
     break
    fi
    break
   fi
 done
 if test "$ac_qt_libs" = "" -o "$ac_qt_includes" = "";then
   AC_MSG_RESULT([no])
 fi 
 AC_MSG_RESULT([yes])
 CPPFLAGS="${CPPFLAGS} -I$ac_qt_includes "
 if ! test "$ac_qt_libs" = ""; then
   LDFLAGS="${LDFLAGS} -L$ac_qt_libs"
 fi 
 QTLIBS="-lqt"
 AC_SUBST(QTLIBS)
 ]
 AC_DEFINE(HAVE_QT))
