dnl ----------------------------------------------------------------------
dnl
dnl CXX_HAVE_SWAP_IN_STL_ALGORITHM
dnl ---------------------------------------------
dnl
dnl If the C++ compiler have swap in stl
dnl                         define `HAVE_SWAP_IN_STL_ALGOBASE'.
dnl
dnl This program is free software; you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation; either version 2 of the License, or
dnl  (at your option) any later version.
dnl  
dnl  This program is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with this program; if not, write to the Free Software
dnl  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

AC_DEFUN(CXX_HAVE_SWAP_IN_STL_ALGOBASE,
[
AC_REQUIRE([AC_PROG_CXX])
AC_MSG_CHECKING(whether the C++ compiler (${CXX}) has swap in stl algorithm)
AC_CACHE_VAL(ddec_cv_have_swap_in_stl_algobase,
[
AC_LANG_SAVE
AC_LANG_CPLUSPLUS
AC_TRY_COMPILE([
#include <algobase.h>
int main()
{
    double a=1,b=2;
    swap(a,b);
}
],[return 1;],
ddec_cv_have_swap_in_stl_algobase=yes,
ddec_cv_have_swap_in_stl_algobase=no)
AC_LANG_RESTORE
])
AC_MSG_RESULT($ddec_cv_have_swap_in_stl_algobase)
if test "$ddec_cv_have_swap_in_stl_algobase" = yes; then
AC_DEFINE(HAVE_SWAP_IN_STL_ALGOBASE)
fi
])dnl
