dnl ----------------------------------------------------------------------
dnl
dnl CXX_HAVE_RESTRICT
dnl ---------------------------------------------
dnl
dnl If the C++ compiler support restrict, 
dnl                         define `HAVE_RESTRICT'.
dnl
dnl This program is free software; you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation; either version 2 of the License, or
dnl  (at your option) any later version.
dnl  
dnl  This program is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with this program; if not, write to the Free Software
dnl  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

AC_DEFUN(CXX_HAVE_RESTRICT,
[
AC_REQUIRE([AC_PROG_CXX])
AC_MSG_CHECKING(whether the C++ compiler (${CXX}) supports restrict)
AC_CACHE_VAL(ddec_cv_have_restrict,
[
AC_LANG_SAVE
AC_LANG_CPLUSPLUS
AC_TRY_COMPILE([
],[int * restrict a;return 0;],
ddec_cv_have_restrict=yes,
ddec_cv_have_restrict=no)
AC_LANG_RESTORE
])
AC_MSG_RESULT($ddec_cv_have_restrict)
if test "$ddec_cv_have_restrict" = yes; then
AC_DEFINE(HAVE_RESTRICT)
else
CXX_HAVE_RESTRICT_EGCS
fi

])dnl

AC_DEFUN(CXX_HAVE_RESTRICT_EGCS,
[
AC_REQUIRE([AC_PROG_CXX])
AC_MSG_CHECKING(whether the C++ compiler (${CXX}) supports __restrict__)
AC_CACHE_VAL(ddec_cv_have_restrict_egcs,
[
AC_LANG_SAVE
AC_LANG_CPLUSPLUS
AC_TRY_COMPILE([
],[int * __restrict__ a;return 0;],
ddec_cv_have_restrict_egcs=yes,
ddec_cv_have_restrict_egcs=no)
AC_LANG_RESTORE
])
AC_MSG_RESULT($ddec_cv_have_restrict_egcs)
if test "$ddec_cv_have_restrict_egcs" = yes; then
AC_DEFINE(HAVE_RESTRICT_EGCS)
fi
])dnl
