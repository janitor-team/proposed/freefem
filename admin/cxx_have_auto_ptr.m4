dnl -*- Mode: m4 -*-
dnl
dnl SUMMARY:      
dnl
dnl AUTHOR:       Christophe Prud'homme
dnl ORG:          Christophe Prud'homme
dnl E-MAIL:       Christophe.Prudhomme@ann.jussieu.fr
dnl
dnl ORIG-DATE:    31-Mar-99 at 00:40:14
dnl LAST-MOD:     13-Jun-00 at 17:52:19 by Christophe Prud'homme
dnl
dnl DESCRIPTION:  
dnl This program is free software; you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation; either version 2 of the License, or
dnl  (at your option) any later version.
dnl  
dnl  This program is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with this program; if not, write to the Free Software
dnl  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
dnl
dnl DESCRIP-END.

dnl
AC_DEFUN(CXX_HAVE_AUTO_PTR,
[
AC_REQUIRE([AC_PROG_CXX])
AC_MSG_CHECKING(whether the C++ compiler (${CXX}) supports auto_ptr<>)
AC_CACHE_VAL(ddec_cv_have_auto_ptr,
[
AC_LANG_SAVE
AC_LANG_CPLUSPLUS
AC_TRY_COMPILE([
#include <memory>

class Date
{
 bool cache;
 public:
  Date():cache(true)  {}
  bool getCache() const throw()
  {
    return cache;
  }
};
typedef auto_ptr<Date> DatePtr;

],
[
    DatePtr d(new Date());
    if (d->getCache())
	return 0;
    else
        return 1;
],
ddec_cv_have_auto_ptr=yes,
ddec_cv_have_auto_ptr=no)
AC_LANG_RESTORE
])
AC_MSG_RESULT($ddec_cv_have_auto_ptr)
if test "$ddec_cv_have_auto_ptr" = yes; then
AC_DEFINE(HAVE_AUTO_PTR)
fi
])dnl
