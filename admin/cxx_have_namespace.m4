dnl -*- Mode: m4 -*-
dnl
dnl SUMMARY:      $Id: cxx_have_namespace.m4,v 1.3 2000/06/13 22:25:33 prudhomm Exp $
dnl
dnl AUTHOR:       Christophe Prud'homme
dnl ORG:          Christophe Prud'homme
dnl E-MAIL:       Christophe.Prudhomme@ann.jussieu.fr
dnl
dnl ORIG-DATE:    26-Dec-98 at 17:50:54
dnl LAST-MOD:     13-Jun-00 at 17:51:54 by Christophe Prud'homme
dnl
dnl DESCRIPTION:  
dnl This program is free software; you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation; either version 2 of the License, or
dnl  (at your option) any later version.
dnl  
dnl  This program is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with this program; if not, write to the Free Software
dnl  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
dnl
dnl If the C++ compiler support namespace, define `HAVE_NAMESPACE'.
dnl DESCRIP-END.


dnl
AC_DEFUN(CXX_HAVE_NAMESPACE,
[
AC_REQUIRE([AC_PROG_CXX])
AC_MSG_CHECKING(whether the C++ compiler (${CXX}) supports namespace header)
AC_CACHE_VAL(ddec_cv_have_namespace,
[
AC_LANG_SAVE
AC_LANG_CPLUSPLUS
AC_TRY_COMPILE([
namespace computers {

class keyboard {
  public:
    int getkey() const;
};

int keyboard::getkey() const
{
    return 0;
}

}

namespace music {

class keyboard {
  public:
    void playNote(int note);
};

}

namespace music {

void keyboard::playNote(int note)
{
}

namespace foo {
  template<class T> void Xeg(T) { }
}
}

using namespace computers;
],
[
    keyboard x;
    int z = x.getkey();

    music::keyboard y;
    y.playNote(z);
    using namespace music::foo;
    Xeg(z);
    return 0;
],
ddec_cv_have_namespace=yes,
ddec_cv_have_namespace=no)
AC_LANG_RESTORE
])
AC_MSG_RESULT($ddec_cv_have_namespace)
if test "$ddec_cv_have_namespace" = yes; then
AC_DEFINE(HAVE_NAMESPACE)
fi
])dnl

dnl
AC_DEFUN(CXX_HAVE_NAMESPACE_STD,
[
AC_REQUIRE([CXX_HAVE_NAMESPACE])
AC_MSG_CHECKING(whether the C++ compiler (${CXX}) supports the namespace std::)
AC_CACHE_VAL(ddec_cv_have_namespace_std,
[
AC_LANG_SAVE
AC_LANG_CPLUSPLUS
AC_TRY_COMPILE([
#include <cstdlib>

using namespace std;
],
[
 std::exit(0);
 return 0;
],
ddec_cv_have_namespace_std=yes,
ddec_cv_have_namespace_std=no)
AC_LANG_RESTORE
])
AC_MSG_RESULT($ddec_cv_have_namespace_std)
if test "$ddec_cv_have_namespace_std" = yes; then
AC_DEFINE(HAVE_NAMESPACE_STD)
fi
])