dnl -*- Mode: m4 -*-
dnl
dnl SUMMARY:      
dnl
dnl AUTHOR:       Christophe Prud'homme
dnl ORG:          Christophe Prud'homme
dnl E-MAIL:       Christophe.Prudhomme@ann.jussieu.fr
dnl
dnl ORIG-DATE:    18-Jan-99 at 10:29:46
dnl LAST-MOD:     13-Jun-00 at 17:52:00 by Christophe Prud'homme
dnl
dnl DESCRIPTION:  
dnl This program is free software; you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation; either version 2 of the License, or
dnl  (at your option) any later version.
dnl  
dnl  This program is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with this program; if not, write to the Free Software
dnl  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
dnl
dnl DESCRIP-END.

dnl
AC_DEFUN(CXX_HAVE_MUTABLE,
[
AC_REQUIRE([AC_PROG_CXX])
AC_MSG_CHECKING(whether the C++ compiler (${CXX}) supports mutable keyword)
AC_CACHE_VAL(ddec_cv_have_mutable,
[
AC_LANG_SAVE
AC_LANG_CPLUSPLUS
AC_TRY_COMPILE([
class Date
{
 mutable bool cache;
public:
 Date(){}
};
],
[
    Date d;
    return 0;
],
ddec_cv_have_mutable=yes,
ddec_cv_have_mutable=no)
AC_LANG_RESTORE
])
AC_MSG_RESULT($ddec_cv_have_mutable)
if test "$ddec_cv_have_mutable" = yes; then
AC_DEFINE(HAVE_MUTABLE)
fi
])dnl
