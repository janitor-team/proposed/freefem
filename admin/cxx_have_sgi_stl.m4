dnl -*- Mode: m4 -*-
dnl
dnl SUMMARY:      
dnl
dnl AUTHOR:       Christophe Prud'homme
dnl ORG:          Christophe Prud'homme
dnl E-MAIL:       Christophe.Prudhomme@ann.jussieu.fr
dnl
dnl ORIG-DATE:    26-Feb-99 at 09:45:31
dnl LAST-MOD:     13-Jun-00 at 17:51:43 by Christophe Prud'homme
dnl
dnl DESCRIPTION:  
dnl This program is free software; you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation; either version 2 of the License, or
dnl  (at your option) any later version.
dnl  
dnl  This program is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with this program; if not, write to the Free Software
dnl  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
dnl DESCRIP-END.


AC_DEFUN(CXX_HAVE_STL_SGI,
[
    AC_MSG_CHECKING([if this is a SGI STL implementation])
    AC_CACHE_VAL(cxx_have_stl_type_sgi,
    [
      AC_TRY_COMPILE([
#include <string>
],[
  string astring="Hallo Welt.";
  astring.erase(0, 6); // now astring is "Welt"
  return 0;
], cxx_have_stl_type_sgi=yes,
   cxx_have_stl_type_sgi=no)
])

   AC_MSG_RESULT($cxx_have_stl_type_sgi)

   if test "$cxx_have_stl_type_sgi" = "yes"; then
	AC_DEFINE_UNQUOTED(HAVE_SGI_STL) 
   fi
])
