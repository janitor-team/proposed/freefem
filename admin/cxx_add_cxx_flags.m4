dnl -*- Mode: m4 -*-
dnl
dnl SUMMARY:      
dnl
dnl AUTHOR:       Christophe Prud'homme
dnl ORG:          Christophe Prud'homme
dnl E-MAIL:       Christophe.Prudhomme@ann.jussieu.fr
dnl
dnl ORIG-DATE:    26-Apr-99 at 20:44:49
dnl LAST-MOD:     13-Jun-00 at 17:52:24 by Christophe Prud'homme
dnl
dnl DESCRIPTION: 
dnl This program is free software; you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation; either version 2 of the License, or
dnl  (at your option) any later version.
dnl  
dnl  This program is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with this program; if not, write to the Free Software
dnl  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
dnl
dnl DESCRIP-END.


dnl
AC_DEFUN(CXX_ADD_CXX_FLAGS,
[
dnl
dnl gives some extra warning if under gcc/g++
dnl
if test "$CXX" = "g++" || test "$host_os" = "linux" ; then
  CXXFLAGS="${CXXFLAGS}  -ftemplate-depth-30 -fstrict-aliasing"
  if test $ac_cv_prog_cxx_g = yes; then
        CXXFLAGS="${CXXFLAGS} -g3 -ggdb"
  fi
fi

dnl
dnl gives some extra warning if aCC
dnl
if test "$CXX" = "aCC"; then
  CXXFLAGS="+inst_close +O2 -s +Olibcalls"
fi

dnl
dnl gives some extra warning if KCC
dnl
if test "$CXX" = "KCC"; then
  CXXFLAGS="--restrict --strict_warnings"
  if test $ac_cv_prog_cxx_g = yes; then
        CXXFLAGS="${CXXFLAGS} -g +K0"
  fi
  CPPFLAGS="${CPPFLAGS} -DDDEC_DEFINE_NAMESPACE_STD=1"
fi
])