// -*- Mode: c++ -*-
//
// SUMMARY:      
// USAGE:        
//
// ORG:          Christophe Prud'homme
// AUTHOR:       Christophe Prud'homme
// E-MAIL:       Christophe.Prudhomme@ann.jussieu.fr
//
// DESCRIPTION:
// 
// < description of the code here>
//  
// DESCRIP-END.
//
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>

// c++ include
#include <new>
#include <iostream>

// libfem
#include <femParser.hpp>

#ifdef HPPA
#ifndef __GNUC__
typedef char *caddr_t;
#endif
#endif

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>


/*
 * this is for HP workstations
 * this allow to avoid including setjmp.h
 *
 * SEE THE FUNCTION ERREUR (INT)
 */
#if ( defined (__hp9000s700) )
# ifdef __cplusplus
extern "C" {
# endif /* __cplusplus */
# define _JBLEN 50
  typedef double jmp_buf[_JBLEN/2];
  extern void longjmp (jmp_buf,int);
  extern int setjmp (jmp_buf);
# ifdef __cplusplus
}
# endif /* __cplusplus */
#else /* not __hp9000s700 */
# include <setjmp.h>
#endif /* defined (__hp9000s700) */
 

#define reel float 


typedef enum {
  NO_MESSAGE=-2,
  STOP = -1,
  ERROR = 0,
  MESH = 1,
  FUNCTION = 2
} compil_msg;
extern compil_msg cmsg;

void
out_of_memory ()
{
  std::cerr << "FreeFEM error: operator new failed; not enough memory" << std::endl;
#if defined(XGFEM)
  SlaveDeconnect ();
#endif /* XGFEM */
  exit (-1);
}
void
NEW_HANDLER (void)
{
  std::set_new_handler (&out_of_memory);
}

int getprog(char* fn,int argc, char **argv)
{
  if (argc != 2) return 0;
  strcpy(fn,argv[1]);
  printf(" file : %s\n",fn);
  return argc;
}

char *
readprog (char *path)
{
  long count;
  int l = 1;
  FILE *f;
  char *tmp = NULL;

  if ((f = fopen (path, "r")) == NULL)
    {
      fprintf (stderr, "Freefem::readprog error : Cannot read %s\n", path);
      exit(-1);
    }
  count = 0;
  while (!feof (f))
    {
      fgetc (f);
      count++;
    }
  rewind (f);
  tmp = new char[count + 255];
  memset(tmp, 0, (count + 255)*sizeof(char));
  tmp[0] = '{';			/* place program between braces */
  while (!feof (f))
    {
      fgets (tmp + l, 255, f);
      /*fputs (tmp + l, stdout);*/
      l = strlen (tmp);
    }
  tmp[l] = '}';
  l++;
  fclose (f);

  return tmp;
}

int
main (int argc, char **argv)
{
  
#if 1
  char       *fname;

  NEW_HANDLER (); // see dependent system files ({pc,x,mac}rgraph.{h,cpp})
  
  fname = new char[256];

  argc = getprog (fname, argc, argv);
  if (argc == 2)
    {
      char* __prog = readprog (fname);
      
      delete [] fname;

      
      fem::femParser* parser = fem::femParser::New();
      parser->setText( __prog );
      parser->parse();
      delete parser;
    }
  else
    {
      std::cerr << "Usage:\n"
		<< "  freefem filename.pde\n";
    }
#endif
}
