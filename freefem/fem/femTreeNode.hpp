// -*- Mode: c++ -*-
//
// SUMMARY:      
// USAGE:        
//
// ORG:          Christophe Prud'homme
// AUTHOR:       Christophe Prud'homme
// E-MAIL:       prudhomm@users.sourceforge.net
//
// DESCRIPTION:
/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
// 
// < description of the code here>
//  
// DESCRIP-END.
//

#ifndef __noeud_H
#ifdef __GNUG__
#pragma interface
#endif
#define __noeud_H 1

#if defined(HAVE_CONFIG_H)
#include <config.h>
#endif /* HAVE_CONFIG_H */


#include <femLexical.hpp>

namespace fem
{
DECLARE_CLASS( noeud );

  
  /*!
    \class noeud

   
    @author Christophe Prud'homme <Christophe.Prudhomme@ann.jussieu.fr>
    @see
    @version #$Id: femTreeNode.hpp,v 1.1 2001/06/26 17:37:54 prudhomm Exp $#
  */

  class noeud
  {
  public:


    /** Typedefs
     */
    //@{
  
  
    //@}
  
    /** Constructors, destructor and methods
     */ 
    //@{
  
    noeud();
    noeud(const noeud&);
    ~noeud();
  
    Symbol symb;
    creal value;
    ident *name;
    long  junk;
    char  *path;
    struct noeud *l1, *l2, *l3, *l4;

    //@}
  };
}
#endif /* __noeud_H */
