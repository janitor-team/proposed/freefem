// -*- Mode: c++ -*-
//
// SUMMARY:      
// USAGE:        
//
// ORG:          Christophe Prud'homme
// AUTHOR:       Christophe Prud'homme
// E-MAIL:       prudhomm@users.sourceforge.net
//
// DESCRIPTION:
/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
// 
// < description of the code here>
//  
// DESCRIP-END.
//
#if defined(__GNUG__)
#pragma implementation
#endif /* __GNUG__ */


#include <iostream>
#include <femMisc.hpp>
#include <femFunction.hpp>

namespace fem
{
function::function()
  :
  __name( 0 ),
  __arg1( 0 ),
  __arg2( 0 ),
  __def( 0 )
{
}

function::function( function const& f )
  :
  __name( f.__name ),
  __arg1( f.__arg1 ),
  __arg2( f.__arg2 ),
  __def( f.__def )

{
}
function::~function()
{
}
function&
function::operator=( function const& f )
{
  if ( &f != this )
    {
      __name =  f.__name;
      __arg1 =  f.__arg1;
      __arg2 =  f.__arg2;
      __def = f.__def;
    }
  return *this;
}
} 
