// -*- Mode : c++ -*-
//
//   SUMMARY: 
//     USAGE:
//
//    AUTHOR: Christophe Prud'homme (Jesunix) <prudhomm@mit.edu>
//       ORG: MIT
//    E-MAIL: prudhomm@mit.edu
//
// ORIG-DATE: 12-Jul-01 at 09:43:17
//  LAST-MOD: 12-Jul-01 at 09:43:37 by 
//
// RCS Infos:
// ==========
//    Author: $Author: prudhomm $
//        Id: $Id: femCommon.hpp 206 2006-07-30 16:52:02Z prudhomm $
//  Revision: $Revision: 206 $
//      Date: $Date: 2006-07-30 18:52:02 +0200 (Sun, 30 Jul 2006) $
//    locker: $Locker:  $
//
//
// DESCRIPTION:
// ============
/*! \file femCommon.hpp

describe femCommon.hpp here

\par Distributed under the GPL(GNU Public License):
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  \par
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  \par
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */
// DESCRIP-END.
//
#ifndef __femCommon_H
#define __femCommon_H 1

#define DECLARE_TYPE( mydecl, mytype )  \
typedef mydecl         mytype;         \
typedef mytype *       mytype ## Ptr;  \
typedef const mytype * mytype ## Cptr; \
typedef mytype &       mytype ## Ref;  \
typedef const mytype & mytype ## Cref;

/**
Declare class , class pointer , 
const pointer, class reference 
and const class reference types for classes. For example
DECLARE_CLASS( Exception );
@param tag The class being declared
*/

#define DECLARE_CLASS( tag )            \
   class   tag;                        \
   typedef tag *       tag ## Ptr;     \
   typedef const tag * tag ## Cptr;    \
   typedef tag &       tag ## Ref;     \
   typedef const tag & tag ## Cref;


#endif /* __femCommon_H */
