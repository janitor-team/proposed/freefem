// Emacs will be in -*- Mode: c++ -*-
//
// ********** DO NOT REMOVE THIS BANNER **********
//
// SUMMARY: Language for a Finite Element Method
//
// AUTHORS:  C. Prud'homme
// ORG    :          
// E-MAIL :   prudhomm@users.sourceforge.net
//
// ORIG-DATE:     June-94
// LAST-MOD: 12-Jul-01 at 10:00:32 by 
//
// DESCRIPTION:
/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
// DESCRIP-END.
//

#ifndef __TRIANGUL_H
#define __TRIANGUL_H

#include <femCommon.hpp>

namespace fem
{
  //DECLARE_CLASS( femMesh );

  /*!
    \class femMesh
    \brief Mesh class

    Finite element mesh using P1 elements(triangles).
    
    \author Christophe Prud'homme <prudhomm@users.sourceforge.net>
    \version $Id: femMesh.hpp,v 1.3 2001/07/12 15:15:39 delpinux Exp $
  */
  class femMesh
  {
  public:

    /** @name Typedefs
     */
    //@{

    //! integer type
    typedef long integer;
    
    //! logical
    typedef long logical;

    //! node type
    typedef float femPoint[2];
    
    //! cell type
    typedef long femTriangle[3];
  
    //@}
    /** @name Constructors and destructor
     */
    //@{
    femMesh();
    femMesh( femMesh const& );
    //@}
    
    /** @name Accessors
     */
    //@{

    int getNumberOfPoints() const
    {
      return np;
    }

    int getNumberOfCells() const
    {
      return nt;
    }
    //@}

    /** @name Mutators
     */
    //@{

    void setDimensions( int, int );

    //@}

    /** @name Methods
     */
    //@{

    //! delete the mesh
    void Delete()
    {
      if (rp)
	{
	  delete [] rp;
	  rp = NULL;
	} 
      if (tr)
	{
	  delete [] tr;
	  tr = NULL;
	} 
      if (ng)
	{
	  delete [] ng;
	  ng = NULL;
	}
      if (ngt)
	{
	  delete [] ngt;
	  ngt = NULL;
	}
    }
    
    //! 
    void removeBdyT();

    //! prepare the mesh for generation
    long  create (long nbs, long nbsmax, long nba,
		  float *crbdy, float *hbdy, long *arete, int *ngbdy, long *sd, long nbsd, int* flag,
		  int fflag);

    //! mesh generator
    int mshptg_(float *cr,float *h, long *c, long *nu, long *nbs, long nbsmx, long *tri,
		long *arete, long nba, long *sd,
		long nbsd, long *reft, long *nbt, float coef, float puis, long *err);

    //! check the mesh
    int check( float*, int ) const;

    //@}
  public:
    femPoint* rp;
    femTriangle* tr;
    int* ngt;
    int* ng;
    
  private:

    void swapWithNeibhor( int );
    int renumerotate();
  
    int             mshrgl_ (float *c, long *nrfs, long *nbs, long *nu, long *w1,
			     long *w, float omega, long itermx, float eps);
    int             mshopt_ (long *c, long *nu, long *t, long a, long *err);
    void            mshvoi_ (long *nu, long *w1, long *w, long *nbt, long *nbs);
    int             msha1p_ (long *t, long *s, long *c, long *nu, long *reft, long *tete, long *nbt,
			     long *err);
    int             mshtri_ (float *cr, long *c, long *nbs, long *tri, long *nu, float *trfri, long *err);
    int             mshcxi_ (long *c, long *nu, long *tri, long *nbs, long *tete, long *err);
    int             mshfrt_ (long *c, long *nu, long *nbs, long *arete, long nba, long *sd,
			     long nbsd, long *reft, long *w, long *err);
    int             mshgpt_ (long *c, float *cr, long *nu, float *h, long *reft, long *nbs,
			     long nbsmx, long *nbt, float coef, float puis, float *trfri, long *err);
    long            mshlcl_ (long *c, long *nu, long *tete, long *s);
    int             mshtr1_ (long *criter, long *record, long *n);
    int             mshcvx_ (long direct, long *c, long *nu, long *pfold, long *err);
    int             mshfr1_ (long *c, long *nu, long *it1, long *ita, long *is1, long *s2, long *err);
    int             mshfr2_ (long *c, long *nu, long *lst, long *nbac, long *t, long *ta,
			     long *ss1, long *ss2, long *err);
  
    int             gibbs1_ (integer * n, integer * record, integer * ptvois);
    int             gibbs2_ (integer * n, integer * record, integer * criter);
    int             gibbsa_ (integer * n, integer * ptvois, integer * vois, integer * r, integer * m,
			     integer * nv, integer * nx, integer * ny, integer * nn, integer * w1, integer * w2,
			     integer * pfold, integer * pfnew, integer * impre, integer * nfout);
    int             gibbsb_ (integer * x, integer * y, integer * n, integer * ptvois,
			     integer * vois, integer * nx, integer * ny, integer * nv, integer * nn, integer * m,
			     integer * wh, integer * wl, integer * r, integer * impre, integer * nfout);
    int             gibbsc_ (integer * nz, integer * nv, integer * niveau, integer * n, integer *);
    int             gibbsd_ (integer * racine, integer * n, integer * ptvois, integer *
			     vois, integer * nv, integer * r, integer * niveau);
    int             gibbst_ (integer * n, integer * p, integer * nv, integer * nn, integer * ptvois, integer * vois,
			     integer * m, integer * r, integer * new_, integer * option,
			     integer * pfnew, integer * impre, integer * nfout);
    int             gibbsv (integer * ptvoi, integer * vois, integer * lvois, integer * w, integer * v);

  private:
    long np, nt;
    
  };

}

#endif /* __Triangul_H */
