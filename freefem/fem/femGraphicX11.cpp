//
// SUMMARY:      Xrgraph.c graphics under X11
// USAGE:        
//
// AUTHOR:       Christophe Prud'Homme
// ORG:
// E-MAIL: prudhomm@users.sourceforge.net
// ORIG-DATE:     10-Aug-95
// LAST-MOD:  8-Apr-02 at 00:46:04 by Christophe Prud'homme
// DESCRIPTION:
/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

//routines for X11 and workstations + system dependent routines
// DESCRIP-END.
//

#define _sys_time_h
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <femMisc.hpp>
#include <femGraphic.hpp>
#include <femDisk.hpp>
#include <femLexical.hpp>

#ifdef HPPA
#ifndef __GNUC__
typedef char *caddr_t;
#endif
#endif

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>

namespace fem
{
/*
 * this is for HP workstations
 * this allow to avoid including setjmp.h
 *
 * SEE THE FUNCTION ERREUR (INT)
 */
#if ( defined (__hp9000s700) )
# ifdef __cplusplus
extern "C" {
# endif /* __cplusplus */
# define _JBLEN 50
  typedef double jmp_buf[_JBLEN/2];
  extern void longjmp (jmp_buf,int);
  extern int setjmp (jmp_buf);
# ifdef __cplusplus
}
# endif /* __cplusplus */
#else /* not __hp9000s700 */
# include <setjmp.h>
#endif /* defined (__hp9000s700) */


#define reel float 

jmp_buf context;

//extern int NumOfIterations,Iter;
//extern femMesh t;
//extern float xfmin, xfmax;

static reel echx,echy,rxmin,rxmax,rymin,rymax;
static int width, height, currx, curry;


typedef enum {
  NO_MESSAGE=-2,
  STOP = -1,
  ERROR = 0,
  MESH = 1,
  FUNCTION = 2
} compil_msg;
compil_msg cmsg;

#if defined(KDE_SUPPORT2)

#include <GFem.h>


/*
 * global GFem class 
 * provides general services like:
 *  - graphics routines with really simple interfaces
 *  - total encapsulation of the langage/solver/mesh tools
 */
GFem *gf;

void
run_gfem(GFem *gfem)
{
  gf = gfem;
  
  /* let's solve the problem now */
  gf->run();
}

void
out_of_memory ()
{
  cerr << "FreeFEM error: operator new failed; not enough memory" << endl;
  exit (-1);
}

void message(char *s)
{
  QString buf(s);
  buf += "\n",
  QMessageBox::message ("Message", 
		      (const char *)buf,
		      "Ok");
}

void erreur(char *s)
{
  extern jmp_buf context;
  message(s);
  cmsg = ERROR;
  longjmp (context, -1);
}

void rflush()
{
  
  gf->flush();
}

void rattente(int waitm )
{
  bool ok = false;
  
  gf->flush();
  gf->closeGraphic();
  if (waitm)
    ok = gf->wait();
  if (!ok)
    {
      extern jmp_buf context;
      cmsg = STOP;
      longjmp (context, -1);
    }
  gf->initGraphic();
}

int xerror()
{
  fprintf(stderr, "Probleme avec X-Windows\n");
  return 1;
}

void initgraphique()
{
  width = gf->width(), height = gf->height();
  gf->initGraphic();
}

void closegraphique()
{
  gf->closeGraphic();
}

void cadre(reel xmin,reel xmax,reel ymin,reel ymax)
{
  rxmin = xmin;
  rxmax = xmax;
  rymin = ymin;
  rymax = ymax;

  echx = width / (xmax - xmin);
  echy = height / (ymax - ymin);
}

void couleur(int c)
{
/*
  setcolor(lacouleur = c);
*/
  
}

int scalx(reel x)
{
  return (int)((x - rxmin) * echx);
}

int scaly(reel y)
{
  return (int)((rymax - y) * echy);
}

void pointe(reel x, reel y)
{
  gf->drawPoint(scalx(x), scaly(y));
}

void rmoveto(reel x, reel y)
{
  gf->moveTo(scalx(x), scaly(y));
}

void rlineto(reel x, reel y)
{
  gf->lineTo(scalx(x), scaly(y));
}

void 
cadreortho(reel centrex, reel centrey, reel rayon)
{
  
  if (height < width)
  {
    rymin = centrey - rayon;
    rymax = centrey + rayon;
    echx = echy= height / (2 * rayon);
    rxmin= centrex - width / (2 * echx);
    rxmax= centrex + width / (2 * echx);
  }
  else
  {
    rxmin = centrex - rayon;
    rxmax = centrex + rayon;
    echx = echy = width / (2 * rayon);
    rymin = centrey - height / (2 * echy);
    rymax = centrey + height / (2 * echy);
  }
}

void cercle(reel centrex, reel centrey, reel rayon)
{
  
}
void reffecran()
{
  gf->clear();
}

void raffpoly(int n, float *poly)
{
  int i, *poly0;
  
  poly0=new int[2*n];
  if(poly0 != NULL)
  {
    fprintf(stderr, "Erreur d'allocation dans raffpoly\n");
    return;
  }
  for(i=0; i<n; i++)
  {
    poly0[2*i]=scalx(poly[2*i]);
    poly0[2*i+1]=scaly(poly[2*i+1]);
  }
/*  fillpoly(n, poly0); */
  delete [] poly0;
}

int getprog(char* fn,int argc, char **argv)
{
  if (argc != 2) return 0;
  strcpy(fn,argv[1]);
  printf(" file : %s\n",fn);
  return argc;
}

void execute (char * str)
{ 
  system(str);
}



#else

static  Display *display;
static  Window win;
static  XSizeHints size_hints;
static  GC gc;
static  XFontStruct *font_info;


void message(char *s)
{
  printf("%s \n",s);
}

void erreur(char *s)
{
  jmp_buf context;
  message(s);
  cmsg = ERROR;
#if defined(XGFEM)
  SlaveSendError ((int&)cmsg, s, numligne);
  SlaveDeconnect ();
#endif /* XGFEM */
  longjmp (context, -1);
}

void rflush()
{
  XFlush(display);
}

void rattente(int waitm , float xfmin, float xfmax)
{
  char click[] = "Click to continue...";
  char values[256];
  XEvent report;
  
  if ( std::abs(xfmin) > 1e-7 || std::abs(xfmax) > 1e-7 )
    {
      sprintf(values, "Mini/Maxi: [ %f , %f ]", xfmin, xfmax);
      XDrawString (display,
                   win,
                   gc,
                   5,40,
                   values,
                   strlen(values));
    }
#if 0
  if (NumOfIterations > 0)
    {
      sprintf(values, "Iter: %d", Iter);
      XDrawString (display,
                   win,
                   gc,
                   5,60,
                   values,
                   strlen(values));
    }
#endif
  waitm = 1;
  if (waitm)
    {
      XDrawString (display,
                   win,
                   gc,
                   5,20,
                   click,
                   strlen(click));
      do XNextEvent(display, &report);
      while (report.type != ButtonPress && report.type != KeyPress);
    }
  XCheckMaskEvent(display, ButtonPressMask,&report);
  if (report.type == ButtonPress)
    if (report.xbutton.button == Button3)
      {
        cmsg = STOP;
#if defined(XGFEM)
        slave.csend ((int)cmsg);
        cmsg = NO_MESSAGE;
        slave.csend ((int)cmsg);
        slave.cclose(1);
#endif /* XGFEM */
        closegraphique();
        longjmp (context, -1);
      }
  XFlush (display);
}

int xerror()
{
  fprintf(stderr, "Probleme avec X-Windows\n");
  return 1;
}

void initgraphique()
{
  XEvent report;
  XSetWindowAttributes attrs;
  unsigned long attrs_mask;

  display = XOpenDisplay(NULL);



  if ((font_info = XLoadQueryFont(display, "7x13")) == NULL)
   {
    fprintf (stderr, "FreeFEM: cannot open 7x13 font\n");
    fprintf (stderr, "FreeFEM: I am going to try an other one.\n");
    if ((font_info = XLoadQueryFont(display, "9x15")) == NULL)
	{
	  fprintf (stderr, "FreeFEM: cannot open 9x15 font\n");
          fprintf (stderr, "FreeFEM: AArgghh no misc font.\n");
        }
   }

  XSetErrorHandler((XErrorHandler)xerror);
  XSetIOErrorHandler((XIOErrorHandler)xerror);
  int screen = DefaultScreen(display);
  width = DisplayWidth(display, screen) - 100;
  height = DisplayHeight(display, screen) - 160;
  win = XCreateSimpleWindow(display, RootWindow(display, screen), 50, 80,
                            width, height, 4, 
                            BlackPixel(display, screen), WhitePixel(display, screen));

  size_hints.flags = PPosition | PSize;
  size_hints.x = 0;
  size_hints.y = 0;
  size_hints.width = width;
  size_hints.height = height;

  XSetStandardProperties(display, win, "ploth", NULL, 0, NULL, 0, &size_hints);
  XSelectInput(display, win, ExposureMask | ButtonPressMask);
  gc = XCreateGC(display, win, 0, NULL);
  XSetFont(display, gc, font_info->fid);
  XSetForeground(display, gc, BlackPixel(display, screen)); 
  XMapWindow(display, win);

  //
  // enable backing store
  // WARNING: not all display are able to do backing 
  //
  attrs.backing_store = DoesBackingStore (ScreenOfDisplay(display,screen));
  attrs_mask = CWBackingStore;
  XChangeWindowAttributes (display, win, attrs_mask, &attrs);
    
  do XNextEvent(display, &report); while (report.type != Expose);
}

void closegraphique()
{
  XUnloadFont(display, font_info->fid);
  XFreeGC(display, gc);
  XCloseDisplay(display);
}

void cadre(reel xmin,reel xmax,reel ymin,reel ymax)
{
  rxmin = xmin;
  rxmax = xmax;
  rymin = ymin;
  rymax = ymax;

  echx = width / (xmax - xmin);
  echy = height / (ymax - ymin);
}

void couleur(int c)
{
/*
  setcolor(lacouleur = c);
*/
}

int scalx(reel x)
{
  return (int)((x - rxmin) * echx);
}

int scaly(reel y)
{
  return (int)((rymax - y) * echy);
}

void pointe(reel x, reel y)
{
  XDrawPoint(display, win, gc, scalx(x), scaly(y));
}

void rmoveto(reel x, reel y)
{
  currx = scalx(x);
  curry = scaly(y);
}

void rlineto(reel x, reel y)
{
  int newx = scalx(x), newy = scaly(y);
  XDrawLine(display, win, gc, currx, curry, newx, newy);
  currx = newx; curry = newy;
/*   XFlush(display); */
}

void cadreortho(reel centrex, reel centrey, reel rayon)
{

  if (height < width)
  {
    rymin = centrey - rayon;
    rymax = centrey + rayon;
    echx = echy= height / (2 * rayon);
    rxmin= centrex - width / (2 * echx);
    rxmax= centrex + width / (2 * echx);
  }
  else
  {
    rxmin = centrex - rayon;
    rxmax = centrex + rayon;
    echx = echy = width / (2 * rayon);
    rymin = centrey - height / (2 * echy);
    rymax = centrey + height / (2 * echy);
  }
}

void cercle(reel centrex, reel centrey, reel rayon)
{
  int r = (int)(rayon * echx);
  XDrawArc(display, win, gc,
     scalx(centrex) - r, scaly(centrey) - r, width, height, 0, 360 * 64);
  XFlush(display);
}
void reffecran()
{
 XClearWindow(display,win);
}

void raffpoly(int n, float *poly)
{
  int i, *poly0;
  
  poly0=new int[2*n];
  if(poly0 != NULL)
  {
    fprintf(stderr, "Erreur d'allocation dans raffpoly\n");
    return;
  }
  for(i=0; i<n; i++)
  {
    poly0[2*i]=scalx(poly[2*i]);
    poly0[2*i+1]=scaly(poly[2*i+1]);
  }
/*  fillpoly(n, poly0); */
  delete [] poly0;
}


void execute (char * str)
{ 
  system(str);
}
}
#endif /* KDE_SUPPORT */
