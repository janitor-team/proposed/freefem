// -*- Mode: c++ -*-
//
// SUMMARY:      
// USAGE:        
//
// ORG:          Christophe Prud'homme
// AUTHOR:       Christophe Prud'homme
// E-MAIL:       prudhomm@users.sourceforge.net
//
// DESCRIPTION:
/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
// 
// < description of the code here>
//  
// DESCRIP-END.
//
#ifndef __femident_H
#ifdef __GNUG__
#pragma interface
#endif
#define __femident_H 1

#include <femCommon.hpp>


#include <femMisc.hpp>

namespace fem
{
typedef enum
{
  lpar, rpar,   lbrace, rbrace, cste,   newvar, oldvar, op_plus,   op_minus,  star,
  slash, modulo, lt,  le,   gt,   ge,   eq,   neq,  comma,  semicolon, colon,sine, 
  cosine,logarithm, exponential, root, absolute, expo, acose, asine, tane, et,
  coshe, sinhe, tanhe, ou, mini, maxi, partial_x, partial_y, si, alors, 
  autrement, loop, error, _end, becomes, fdecl, symb_bdy, symb_build, atane, fctdef,
  symb_solv, symb_dch, symb_frr, symb_id, symb_lapl, symb_div, trace,
  changewait,trace3d, chaine,  
  sauve, charge, sauvmsh, chargmsh, arret, fctfile, div_x, div_y, symb_convect,evalfct,
  symb_exec, sauvetout, symb_user, partreal, partimag, symb_system, symb_pde,
  id_bdy, dnu_bdy,  
  d_xx, d_xy, d_yy, d_yx, symb_complex, symb_precise, prodscal, one, wait_state,
  nowait, nographics, rhsconvect,adaptmesh,polygon,gint,bint,bracketr,bracketl,varsolve,penall
} Symbol;

DECLARE_CLASS( ident );


/*!
  \class ident

  
  \author Christophe Prud'homme <prudhomm@users.sourceforge.net>
  \version $Id: femIdentifier.hpp,v 1.3 2001/07/12 15:15:39 delpinux Exp $
*/
class ident
{
public:
  
  /** Typedefs
   */
  //@{


  //@}

  /** Constructors, destructor and methods
   */ 
  //@{

  //! default constructor
  ident();
  //! copy constructor
  ident(ident const&);
  //! destructor
  ~ident();
  //! copy operator
  ident& operator=( ident const& );
  
  
  char *name;
  Symbol symb;
  creal value;
  creal *table;
  //@}

private:

  
};
}
#endif /* __ident_H */

