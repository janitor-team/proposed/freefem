// -*- Mode: c++ -*-
//
// SUMMARY:      
// USAGE:        
//
// ORG:          Christophe Prud'homme
// AUTHOR:       Christophe Prud'homme
// E-MAIL:       prudhomm@users.sourceforge.net
//
// DESCRIPTION:
/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
// 
// < description of the code here>
//  
// DESCRIP-END.
//

#ifdef __GNUG__
#pragma interface
#endif

#include <femIdentifier.hpp>
#include <cstring>

namespace fem
{
  ident::ident()
    :
    name ( 0 ),
    symb ( lpar ),
    value ( 0 ),
    table ( 0 )
  {
  }
  ident::ident( ident const& i )
    :
    name( new char[strlen(i.name) + 1] ),
    symb( i.symb ),
    value( i.value ),
    table( new creal )
  {
    strcpy( name, i.name );
    *table = *i.table;
  }
  ident::~ident()
  {
  }
  ident&
  ident::operator=( ident const& i ) 
  {
    if ( &i != this )
      {
	name =  new char[strlen(i.name) + 1];
	symb = i.symb;
	value = i.value;
	table =  new creal;
      
	strcpy( name, i.name );
	*table = *i.table;
      }
  
    return *this;
  
  }
}
