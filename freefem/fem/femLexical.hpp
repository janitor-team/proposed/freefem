// Emacs will be in -*- Mode: c++ -*-
//
// ********** DO NOT REMOVE THIS BANNER **********
//
// SUMMARY: Language for a Finite Element Method
//     
//
// AUTHORS:  C. Prud'homme
// ORG    :          
// E-MAIL :   prudhomm@users.sourceforge.net
//
// ORIG-DATE:     June-94
// LAST-MOD:     13-Aug-00 at 22:42:38 by Christophe Prud'homme
//
// DESCRIPTION:  
/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
// DESCRIP-END.
//

#ifndef __LEXICAL_H
#define __LEXICAL_H

#define MAXIDENTS 200

#include <femIdentifier.hpp>

namespace fem
{

typedef struct
{
 int bdy, build, onbdy, solv, fct, si, eq, param,t,fem, syst, complexe, precise, graphics;
} drapeaux;

typedef struct programme
{
 char *thestring, *curchar;
 Symbol sym;
 int numligne;
 struct programme *pere;
} programme;

extern int numligne;
extern char *thestring, *curchar;
extern drapeaux flag;
extern Symbol cursym;
extern float curcst;
extern ident *curident;
extern char curchaine[100];
extern int numidents;
extern ident idents[MAXIDENTS];
extern programme *curprog;

long wherearewe();
void initlex(const char *);
void nextsym(void);
void closelex();
}
#endif /* __LEXICAL_H */
