// -*- Mode: c++ -*-
//
// SUMMARY:      
// USAGE:        
//
// ORG:          Christophe Prud'homme
// AUTHOR:       Christophe Prud'homme
// E-MAIL:       prudhomm@users.sourceforge.net
//
// DESCRIPTION:
/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

// 
// < description of the code here>
//  
// DESCRIP-END.
//
#ifndef __function_H
#ifdef __GNUG__
#pragma interface
#endif
#define __function_H 1

#if defined(HAVE_CONFIG_H)
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <femTreeNode.hpp>

namespace fem
{
  ///
  class function
  {
  public:
    /**
       @name Explanation

       <Describe class here>

       @author Christophe Prud'homme <Christophe.Prudhomme@ann.jussieu.fr>
       @see
       @version #$Id: femFunction.hpp,v 1.1 2001/06/26 17:37:54 prudhomm Exp $#
    */


    /** Typedefs
     */
    //@{


    //@}

    /** Constructors, destructor and methods
     */ 
    //@{

    function();
    function( function const& );
    ~function();
    function& operator=( function const& );
  
    //@}



  private:

    ident* __name;
    ident* __arg1;
    ident* __arg2;
    noeud* __def;
  

  };
}
#endif /* __function_H */
