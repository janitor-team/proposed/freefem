// Emacs will be in -*- Mode: c++ -*-
//
// ********** DO NOT REMOVE THIS BANNER **********
//
// SUMMARY: Language for a Finite Element Method
//
// AUTHORS:  C. Prud'homme
// ORG    :          
// E-MAIL :  prudhomm@users.sourceforge.net
//
// ORIG-DATE:     June-94
// LAST-MOD:     13-Aug-00 at 23:18:21 by Christophe Prud'homme
//
// DESCRIPTION:  
/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
// DESCRIP-END.
//

#ifndef __FEM1_H
#define __FEM1_H 1

namespace fem
{
  float id(const float x);
  //float norm2( float a);
  float norm(const float x, const float y);

  void rhsPDE(int quadra, float*  fw, float*  f, float*  g);
       
  float gaussband (float*  a, float*  x, long n, long bdthl, int first, float eps);

  float pdeian(float*  a, float*  u, float*  f, float*  g, float*  u0, 
  float*  alpha, float*  rho11, float*  rho12, float*  rho21, float*  rho22,
  float*  u1, float*  u2, float*  beta, int quadra, int factorize);
}
#endif /* __FEM1_H */
