// Emacs will be in -*- Mode: c++ -*-
//
// ********** DO NOT REMOVE THIS BANNER **********
//
// SUMMARY: Language for a Finite Element Method
// 
// AUTHORS:  C. Prud'homme
// ORG    :          
// E-MAIL :   prudhomm@users.sourceforge.net
//
// ORIG-DATE:     June-94
// LAST-MOD: 12-Jul-01 at 09:49:43 by 
//
// DESCRIPTION:
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
  
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
  
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// DESCRIP-END.
//

#ifndef __FEMPARSER_H
#define __FEMPARSER_H

#include <list>
#include <femCommon.hpp>

#include <femMesh.hpp>
#include <femDisk.hpp>
#include <femTreeNode.hpp>
#include <femIdentifier.hpp>

namespace fem
{

  /**
     \var refbdy
     \brief defines the maxiumum number of boundaries
  
     \attention \c refbdy is also defined in femParser.cxx. 
     ATTENTION: if you change it
  */
  const int refbdy = 60;


  DECLARE_CLASS(function);
  DECLARE_CLASS(var);
  DECLARE_CLASS(femGraphicDeviceIndependent);
  DECLARE_CLASS(FEM);
  //DECLARE_CLASS( ident );
  class ident;
  DECLARE_CLASS(femMesh);

  /*!
    \struct var
    \brief defines a language variable

    \author Christophe Prud'homme <prudhomm@ann.jussieu.fr>
    \version $Id: femParser.hpp 206 2006-07-30 16:52:02Z prudhomm $
  */
  struct var
  {
    ident *x, *y, *cursom, *t, *ng, *region, *ne, *I, *pi, *nx, *ny;
  };

  extern int OPTION;
  extern var variables;


  DECLARE_CLASS(femParser);

  /*!
    \class femParser
    \brief provides the syntaxical analysis

    The syntaxic analyser works closely with the lexical analyser.
       
  
    \author Christophe Prud'homme <prudhomm@ann.jussieu.fr>
    \version $Id: femParser.hpp 206 2006-07-30 16:52:02Z prudhomm $
  */
  class femParser
  {
  public:

    /** @name Enums
     */
    //@{

    //! graphics type
    enum GraphicsType
    {
      FEM_GRAPHIC    = 0, /**< show the graphics */
      FEM_NO_GRAPHIC = 1  /**< do not show the graphics */
    };
    //@}
    
    /** @name Constructors and destructor
     */
    //@{
    
    //! default constructor
    /**
       \param __t string to be parsed by the analyser
    */
    femParser();

    //! destructor
    ~femParser();
    
    //@}

    /** @name Mutators
     */
    //@{

    //! set the graphic type
    void setGraphicType( GraphicsType gt )
    {
      __graphic_type = gt;
    }
    
    //! set the buffer to be parsed
    void setText( const char* __t )
    {
      if ( __text != 0 )
	{
	  delete[] __text;
	}
      __text = new char[strlen( __t )+1];
      strcpy( __text, __t );
    }
    
    //@}
    
    /** @name Methods */
    //@{

    //! create a new instance
    static femParser* New()
    {
      return new femParser;
    }
    
    //! parse the buffer
    void parse();

    //! 
    void chvar(void);

    //!
    noeudPtr instruction(void);

    //! evaluate the node
    creal eval(noeudPtr);

    //! kill the node
    void bucheron(noeudPtr);

    //! free everything
    void libere(void);

    //! show the tree
    void showtree(noeudPtr t);

    //! initialise the parser
    void initsyntax();

    //! free the parameters
    void libereparam(void);

    //! init the parameters
    void initparam();

    //! get the mesh
    inline femMeshCptr getMesh() const
    {
      return &__mesh;
    }
    //@}
    
  private:

    /* femParser.c prototypes for routines not used elsewhere */
    void  plante (noeudPtr *, Symbol, creal, long, ident *, char *, noeud *, noeud *, noeud *, noeud *);
    void      match (Symbol);
    noeudPtr           facteur ();
    noeudPtr           terme ();
    noeudPtr           exprarith ();
    noeudPtr           exprcomp ();
    noeudPtr           expr ();
    noeudPtr           fctfileproc ();
    noeudPtr           symb_bdyproc ();
    noeudPtr           symb_dchproc ();
    noeudPtr           symb_solvproc ();
    noeudPtr           diskmshproc ();
    noeudPtr 			preparesolve ();
    noeudPtr		 	prepvarsolve ();
    noeudPtr 			symb_pdeproc ();
    void			edp (noeudPtr s);
    void 			defbdybypoint (noeudPtr s);
    void            defbdy (noeudPtr);
    void            build (noeudPtr);
    void            maketable (noeudPtr);
    void            conddch (noeudPtr);
    void            condfrr (noeudPtr);
    void            solve (noeudPtr);
    void            oppde (noeudPtr);
    void            sauvefct (noeudPtr);
    void            chargfct (noeudPtr);
    void            plot (noeudPtr);
    void            plot3d (noeudPtr);
    void            chartrig (noeudPtr);
    void            sauvtrig (noeudPtr);
    void            showident (ident *);
    void            showtreeaux (int, noeudPtr);
    void            adapt(noeudPtr);
    void            reinitialize ();
    int 			setgeom (int cursloc, int iloc, int precise);
    void			varpde(noeudPtr s);
    void			doconddch(int i, int cursloc,int iloc,int* ib,noeudPtr s);
    void 			opcondlim (noeudPtr s);
  
  private:

    noeudPtr __tree;

    
    std::list<function>  __function_list;
    femMesh   __mesh;
    femGraphicDeviceIndependentPtr       __graph;
    FEMPtr          __fem;
  
    int OPTION;
    int i123;
  
    int             imdnu, thesgndnu;	// carries info on sign of dnu(u) and address of u
    char*           saveallname;	// file name for saveall
    int             ihowsyst;	// matrix identifyer for the system
    char*           pt;
    fcts            param;
    int             cursloc, cursom, numnoeuds;
    long            nbsd;
    long            nbs, nba;
  
    noeudPtr* noeuds;
    long* sd;
    long* arete;
    int* ngbdy;
    float* cr;
    float* hh;
  
    int NumOfIterations; // used to store the number of iterations (for xgfem)
    int Iter; // Iteration numero
  
    var             variables;
    int             waitm ;

    char* __text;

    GraphicsType __graphic_type;
  };
}


#endif /* __FEMPARSER_H */
// Common rcs information do not modify
// $Author: prudhomm $
// $Revision: 206 $
// $Date: 2006-07-30 18:52:02 +0200 (Sun, 30 Jul 2006) $
// $Locker:  $
