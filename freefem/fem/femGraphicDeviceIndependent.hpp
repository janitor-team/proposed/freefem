// Emacs will be in -*- Mode: c++ -*-
//
// ********** DO NOT REMOVE THIS BANNER **********
//
// SUMMARY: Language for a Finite Element Method
//
// AUTHORS:  C. Prud'homme
// ORG    :          
// E-MAIL : prudhomm@users.sourceforge.net
//
// ORIG-DATE:     June-94
// LAST-MOD:     13-Aug-00 at 22:41:05 by Christophe Prud'homme
//
// DESCRIPTION:  
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
  
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
  
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// DESCRIP-END.
//
#ifndef __ugraph_H
#ifdef __GNUG__
#pragma interface
#endif
#define __ugraph_H 1

#if defined(HAVE_CONFIG_H)
#include <config.h>
#endif /* HAVE_CONFIG_H */


#include <femMesh.hpp>


namespace fem
{
  DECLARE_CLASS( femGraphicDeviceIndependent );

  /*!
    \class femGraphicDeviceIndependent
    \brief device independat graphic class
    
    @author Christophe Prud'homme <prudhomm@users.sourceforge.net>
    @see
    @version $Id: femGraphicDeviceIndependent.hpp,v 1.2 2001/07/12 15:15:39 delpinux Exp $
  */
  class femGraphicDeviceIndependent
  {
  public:
    
    /** @name Typedefs
     */
    //@{
    typedef femMesh::femPoint femPoint;
    typedef femMesh::femTriangle femTriangle;
  

    //@}

    /** @name Constructors, destructor and methods
     */ 
    //@{

    
    femGraphicDeviceIndependent( femMesh* = 0 );
    ~femGraphicDeviceIndependent();

    //@}

    /** @name Mutators
     */
    //@{
    void setTriangulation( femMesh* );
    //@}

     /** @name Methods
     */
    //@{
    void graph3d(float* f, int waitm);
    void showtriangulation(int waitm);
    void equpot(int *ng, float* f, int nl, int waitm);
    void equpotd(int *ng, float* f, int nl, int waitm);
    void showbdy(long nbs,float* cr, long nba, long* arete, float* hh, int waitm);
    //@}

  private:
  
    void Init (femPoint* rp, int ns, char *s);
    void quicksort (float *tb, int *o, int n);
    void initt (void);
    void projection (float *f);
    void contour (int ng[], int coul);
  
  private:

    femMesh* __t;

    int*    ordre;
    float*  table;
    femPoint* proj;
    float   xfmin;
    float   xfmax;
  };
}
#endif /* __FEMGRAPHICDEVICEINDEPENDENT_H */

